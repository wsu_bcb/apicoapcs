# Welcome to the new distribution site of ApicoAP-CS software!
Please note that [our older site](https://code.google.com/p/apicoapcs/) will no longer be maintained.

In order to access the previous and the latest versions of ApicoAP-CS client software, please follow the [Downloads link](https://bitbucket.org/wsu_bcb/apicoapcs/downloads) and click on ApicoAP-CS-v1.zip. In addition to the client software, you'll also find supplementary tables and supplementary user manual under this link.

In order to access the user manual, please follow the [Wiki link](https://bitbucket.org/wsu_bcb/apicoapcs/wiki/Home). Here is a direct link to the software user manual:

[ApicoAP-CS User Manual](https://bitbucket.org/wsu_bcb/apicoapcs/wiki/ApicoAP-CS%20Client%20Software%20User%20Manual)

###Please report any bugs you encountered using the software to gokcen.cilingir@gmail.com###

**You can find more information over the method and the software at the following reference:** 

[Cilingir, Gokcen, and Shira L. Broschat. "Automated Training for Algorithms That Learn from Genomic Data." BioMed research international 2015 (2015).](http://www.hindawi.com/journals/bmri/2015/234236/abs/)